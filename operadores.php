<?php
//extraigo variables
extract ($_REQUEST);
//incluye funciones
include './funciones.php';
?>
<!DOCTYPE 

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Hola  Mundo</title>
		<link rel="stylesheet" href="./css/estilos.css">
		<link rel="stylesheet" href="./css/fonts.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabezera">
				<h1 class="centrado">PHP: Operadores</h1>
			</div>
			<div id="cssmenu">
			  <ul>
			  	<?php
			  	//defino la seccion
			  	$seccion="operadores";
			  	echo menu("./index.php", "home", $seccion);
				echo menu("./acerca.php","acerca",$seccion);
				echo menu("./operadores.php",'operadores',$seccion);
				echo menu("./ciclos.php","ciclos",$seccion);
					echo menu("./admin/index.php","admin",$seccion);
			  	?>
			     
			  </ul>
			</div>
			<div id="contenido">
				<h1>Sentencia IF</h1>
				<?php
				//iniciar codigo phph
				
				$a="indefinido";
				$b="mujer";
				//si a que e hombre, es igual a b que es mujer
					if ($a==$b) 
					{
						//sentencia verdadera
						echo "$a es igual a $b </br>";
						
					} else 
					{
						//sentencia falsa
						echo "$a  no es igual a $b </br>";
					}
				//evaluar con AND se cumplen ambas expresiones
					if (($a=="hombre")and($b=="mujer")) 
					{
						//sentencia verdadera
						echo "Entra a sentencia verdadera AND </br>";
						
					} else 
					{
						//sentencia falsa
						echo "Entra a sentencia flasa AND</br>";
					}
					//evaluar con or basta con que se cumpla una expresion
					if (($a=="hombre")or($b=="mujer")) 
					{
						//sentencia verdadera
						echo "Entra a sentencia verdadera OR</br>";
						
					} else 
					{
						//sentencia falsa
						echo "Entra a sentencia flasa OR</br>";
					}
					
					//orecedencia de operadores
					echo "<h1>Precedencia de operadores</h1>";
					//exponente se calcula con pow(base,ecponente)
					$r=10+20-5/4*pow(8,3)+52;
					
				
					
					echo $r."</br>";	
					$x=3;
					
					$r2=3*(pow($x,2))-(7/4*$x);
					echo $r2."</br>";
					echo "<h1>IF anidado</h1>";
					$a="hombre";
					$b="mujer";
					//condicion para tener un hijo
					if (($a=="hombre")and($b=="mujer")) 
					{
						echo "Pareja normal e hijo</br>";
					} 
					elseif(($a=="hombre")and($b=="hombre")) 
					{
						echo "Hijo adoptado</br>";
					}
					elseif(($a=="mujer")and($b=="mujer")) 
					{
						echo "2 hijos</br>";
					}
					else {
						//no hay nada
						echo "no hay pareja";
					}
					
					echo "<h1>switch</h1>";
					
					$a="5";
					
					switch ($a) {
						case '1':
							echo "es uno</br>";
							break;
						case '2':
							echo "es dos</br>";
							break;
						case '3':
							echo "es tres</br>";
							break;
						case '4':
							echo "es cuatro</br>";
							break;
						case '5':
							echo "es cinco</br>";
							break;
						default:
							echo "no se</br>";
							break;
					}
					
					//resuelto co if
					if($a==1){
						echo "es uno</br>";
					}
					elseif ($a==2) {
						echo "es dos</br>";
					}
					elseif ($a==3) {
						echo "es tres</br>";
					}
					elseif ($a==4) {
						echo "es cuatro</br>";
					}
					elseif ($a==5) {
						echo "es cinco</br>";
					}
					else {
						echo "no se</br>";
					}
				//indico cierre de php	
				?>
				<form action="" method="post">
					<h1>Suma de 2 numeros</h1>
					<?php
						//valido que exista c
							if(isset($c))
							{
								//asignoa $valor lo que traiga c
								$valor= $c;
							}
							else {
								//de lo contrario a valor lo dejo en blanco
								$valor="";
							}
						?>
					<p>Numero1:<input type="number" name="c" 
						value="<?php echo $valor;?>"/>
					</p>
					<p>Numero2:<input type="number" name="d" value="<?php if(isset($d)) echo $d; ?>"/></p>
					<input type="submit" />
				</form>
			
			</div>
			<div id="pie">
				<p class="centrado">Tedos los derechos reservados © ® SA de CV</p>
			</div>
		</div>
	</body>
</html>