<?php
//extraigo variables
extract ($_REQUEST);
//incluye funciones
include './funciones.php';
?>
<!DOCTYPE 

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Hola  Mundo</title>
		<link rel="stylesheet" href="./css/estilos.css">
		<link rel="stylesheet" href="./css/fonts.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabezera">
				<h1 class="centrado">PHP: Operadores</h1>
			</div>
			<div id="cssmenu">
			  <ul>
			     	<?php
			  	//defino la seccion
			  	$seccion="ciclos";
			  	echo menu("./index.php", "home", $seccion);
				echo menu("./acerca.php","acerca",$seccion);
				echo menu("./operadores.php",'operadores',$seccion);
				echo menu("./ciclos.php","ciclos",$seccion);
					echo menu("./admin/index.php","admin",$seccion);
			  	?>
			  </ul>
			</div>
			<div id="contenido">
			<h1>FOR</h1>
			<?php
			//sentecnia for
			//for(inicio; limite; incremento)
			for ($i=1; $i <= 10 ; $i++) { 
				
				echo $i."</br>";
			}
			//decremento
			for ($i=10; $i >= 1 ; $i--) { 
				
				echo $i."</br>";
			}
			?>
			<h1>While</h1>
			<?php
			//while
			$i=1;
			//while(condicion)
			//solo entra si se cumple
			while ($i <= 10) {
				//accion	
				echo $i."</br>";
				//incremento
				$i++;
			
			}
			
			
			?>
			<h1>Tablas de Multiplicar</h1>
			<?php
			//tabla del 3
			for ($i=1; $i <=10 ; $i++) { 
				echo "<p>".$i." X 3 =".($i*3)."</p>";
			}
			//while
			$i=1;
			while ($i <= 10) {
				echo "<p>".$i." X 3 =".($i*3)."</p>";
				$i++;
			}
			
			?>
			<form action="" method="post">
					<h1>Tabla de multiplicar</h1>
					<?php
						//valido que exista c
							if(isset($c))
							{
								//asignoa $valor lo que traiga c
								$valor= $c;
							}
							else {
								//de lo contrario a valor lo dejo en blanco
								$valor="";
							}
						?>
					<p>Que Tabla:<input type="number" name="c" 
						value="<?php echo $valor;?>"/>
					</p>
					<p>Cunatas operaciones:<input type="number" name="d" value="<?php if(isset($d)) echo $d; ?>"/></p>
					<p>De cunto en cuanto:<input type="number" name="tanto" value="<?php if(isset($tanto)) echo $tanto; ?>"/></p>
					<input type="submit" />
				</form>
				<?php
				//tablas dinamicas
				//valido qu emande las variables
				if(isset($c)&&isset($d)&&isset($tanto))
				{
					//$c=5
					//$d=300
					//$tanto=5
					//si existen las variables entra al ciclo de repeticion
					$inicio=$tanto;
					for ($i=1; $i <=$d ; $i++) {
						//obtenemos
						
						if($i==$inicio)
						{
							echo "<p>".$i." X ".$c." =".($i*$c)."</p>";
							//sumarle 5
							$inicio=$inicio+$tanto;
						} 
						
					
					}
				}
				else {
					echo "<p>Por favor llena todos los campos</p>";
				}
				
				//serie fibonaci
				
				$n1=1;
				$n2=1;
				echo $n1."</br>";
				echo $n2."</br>";
				for ($i=1; $i <= 10 ; $i++) { 
					
					$r=$n1+$n2;
					echo $r."</br>";
					$n1=$n2;
					$n2=$r;
					
					
				}
				
				?>
				<h1>Chicharronera</h1>
				<form action="" method="post">
					<h1>Tabla de multiplicar</h1>
					<?php
						//valido que exista c
							if(isset($a))
							{
								//asignoa $valor lo que traiga c
								$valor= $a;
							}
							else {
								//de lo contrario a valor lo dejo en blanco
								$valor="";
							}
						?>
					<p>Valor de A:<input type="number" name="a" 
						value="<?php echo $valor;?>"/>
					</p>
					<p>Valor de B:<input type="number" name="b" value="<?php if(isset($b)) echo $b; ?>"/></p>
					<p>Valor de C:<input type="number" name="c" value="<?php if(isset($c)) echo $c; ?>"/></p>
					<input type="submit" />
				</form>
				<?php
				//hacemos chicarronera
				//comprobamos valores
				if(isset($a)&&isset($b)&&isset($c))
				{
					//validamos que a sea mayor a 0
					if($a>0)
					{
							$r=((-$b)+sqrt(pow($b, 2)-4*($a*$c)))/(2*$a);
							$r2=((-$b)-sqrt(pow($b, 2)-4*($a*$c)))/(2*$a);
							echo $r."</br>";
							echo $r2."</br>";
					}
					else {
					echo "<p>El valor de A tiene que ser mayor a 0</p>";
						}
				
				}
				else {
					echo "<p>Por favor llena todos los campos</p>";
				}
				?>
				
			</div>
			<div id="pie">
				<p class="centrado">Tedos los derechos reservados © ® SA de CV</p>
			</div>
		</div>
	</body>
</html>