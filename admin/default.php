<?php

	extract ($_REQUEST);
	//Inicio la sesión 
	session_start();
	//COMPRUEBA QUE EL USUARIO ESTA AUTENTIFICADO 
	if($_SESSION["autentificado"] != "SI") { 
   	//si no existe, envio a la página de autentificacion 
   	header("Location: index.php"); 
   	//ademas salgo de este script 
	} else {
    //sino, calculamos el tiempo transcurrido
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-m-d H:i:s");
    $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

    //comparamos el tiempo transcurrido
     if($tiempo_transcurrido >= 600) {
     //si pasaron 10 minutos o más
      session_destroy(); // destruyo la sesión
      header("Location: index.php"); //envío al usuario a la pag. de autenticación
      //sino, actualizo la fecha de la sesión
    }else {
    $_SESSION["ultimoAcceso"] = $ahora;
  	 }
	} 
	//incluye funciones
include '../funciones.php';
	$nombre= $_SESSION["nombre"];
?>
<!DOCTYPE 

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Inicio</title>
		<link rel="stylesheet" href="../css/estilos.css">
		<link rel="stylesheet" href="../css/fonts.css">
		<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../js/coin-slider.js"></script>
		<script type="text/javascript" src="../js/coin-slider.min.js"></script>
		<link rel="stylesheet" href="../css/coin-slider-styles.css" type="text/css" />
		
	</head>
	<body>
		<div id="contenedor">
			<div id="cabezera">
				<h1 class="centrado">Default</h1>
			</div>
			<div id="cssmenu">
			  <ul>
			  		<?php
			  		
					  	//defino la seccion
					  	$seccion="default";
					  	echo menu("./default.php", "default", $seccion);
						echo menu("./datos.php", "datos", $seccion);
						echo menu("./index.php","salir",$seccion);
					  	?>
			  </ul>
			</div>
			<div id="contenido">
				<?php
				echo "<h1>Bienbenido ".$nombre."</h1>";
				?>
				
			</div>
			<div id="pie">
				<p class="centrado">Tedos los derechos reservados © ® SA de CV</p>
			</div>
		</div>
	</body>
</html>