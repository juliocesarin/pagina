-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema cpp
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `cpp` ;

-- -----------------------------------------------------
-- Schema cpp
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cpp` DEFAULT CHARACTER SET utf8 ;
USE `cpp` ;

-- -----------------------------------------------------
-- Table `cpp`.`usarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cpp`.`usarios` (
  `idusarios` INT NOT NULL AUTO_INCREMENT,
  `usario` VARCHAR(45) NULL,
  `pass` VARCHAR(45) NULL,
  `status` INT NULL,
  PRIMARY KEY (`idusarios`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cpp`.`personales`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cpp`.`personales` (
  `idpersonales` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `a_paterno` VARCHAR(45) NULL,
  `a_materno` VARCHAR(45) NULL,
  `correo` VARCHAR(80) NULL,
  `direccion` LONGTEXT NULL,
  `status` INT NULL,
  `idusarios` INT NOT NULL,
  PRIMARY KEY (`idpersonales`),
  INDEX `fk_personales_usarios_idx` (`idusarios` ASC),
  CONSTRAINT `fk_personales_usarios`
    FOREIGN KEY (`idusarios`)
    REFERENCES `cpp`.`usarios` (`idusarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
