<?php
//extraigo variables
extract ($_REQUEST);
//incluye funciones
include './funciones.php';
?>
<!DOCTYPE 

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Hola  Mundo</title>
		<link rel="stylesheet" href="./css/estilos.css">
		<link rel="stylesheet" href="./css/fonts.css">
	</head>
	<body>
		<div id="contenedor">
			<div id="cabezera">
				<h1 class="centrado">PHP:</h1>
			</div>
			<div id="cssmenu">
			  <ul>
			  	<?php
			  	//defino la seccion
			  	$seccion="acerca";
			  	echo menu("./index.php", "home", $seccion);
				echo menu("./acerca.php","acerca",$seccion);
				echo menu("./operadores.php",'operadores',$seccion);
				echo menu("./ciclos.php","ciclos",$seccion);
					echo menu("./admin/index.php","admin",$seccion);
			  	?>
		
			  </ul>
			</div>
			<div id="contenido">
				<?php
				//iniciar codigo phph
					//con echo pintamos en pantalla
					echo "<h1>Hola mundo</h1>";
					//defino variables
					$a=10;
					$b=5;
					//hago operacion
					$r=$a+$b;
					//pinto resultado
					echo "<p>Suma de $a + $b</p>";
					echo "<p>".$r."</p>";
					//resta de numero
					$a=100;
					$b=50;
					$resta=$a-$b;
					echo "<p>resta de $a - $b</p>";
					//hago resta directa
					echo '<p>'.($a-$b).'</p>';
					//muestro el resultado de la operacion resta
					echo '<p>'.$resta.'</p>';
					//asigno valores y al mismo tiempo hago operacion
					echo '<p>'.($a=300-$b=400).'</p>';
					//concateno valores (resultado: 300400)
					echo $a.$b."</br>";
					//concateno con un espacio
					echo $a." ".$b;
					
				//indico cierre de php	
				?>
				<form action="" method="post">
					<h1>Suma de 2 numeros</h1>
					<?php
						//valido que exista c
							if(isset($c))
							{
								//asignoa $valor lo que traiga c
								$valor= $c;
							}
							else {
								//de lo contrario a valor lo dejo en blanco
								$valor="";
							}
						?>
					<p>Numero1:<input type="number" name="c" 
						value="<?php echo $valor;?>"/>
					</p>
					<p>Numero2:<input type="number" name="d" value="<?php if(isset($d)) echo $d; ?>"/></p>
					<input type="submit" />
				</form>
				<?php
				
				//valido por medio de isset que existan variables
				if(isset($c)&&isset($d))
				{
					//realizo multi
					$m=suma($c, $d);
				}
				else {
					$m=0;
				}
			
			
				//pinto numeros de formulario
				
				
				echo "<p>El resultado es: $m</p>";
				
				//promedio
				$calif=array(
							'0'=>'8',
							'1'=>'5',
							'2'=>'7',
							'3'=>'8',
							'4'=>'10',
							'5'=>'7',
							'6'=>'8',
							'7'=>'5',
							'8'=>'7'
				);
				
				echo promedio2($calif);
				?>
			</div>
			<div id="pie">
				<p class="centrado">Tedos los derechos reservados © ® SA de CV</p>
			</div>
		</div>
	</body>
</html>